package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "edmundleung" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( " " ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "edmund" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "edmun" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "edmundleung8" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "123456" ) );
	}

	@Test
	public void testIsValidLoginCharactersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "edmund@leung" ) );
	}
	
}

