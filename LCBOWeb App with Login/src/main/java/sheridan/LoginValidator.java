package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if (loginName == null) {
			return false;
		}
		for (int i = 0; i < loginName.length(); i++) {
			if (!(Character.isLetterOrDigit(loginName.charAt(i)))) {
				return false;
			}
		}
		return loginName.length( ) >= 6 ;
	}
}






